## Deploying on DigitalOcean (sort of manually)

1. Create a droplet using the Docker image (make droplet->marketplace->docker image)
    - Use the basic plan with a regular ssd (you can pick other options, but the smallest one is plenty)
    - you don't need block storage, pick any datacenter region
    - if you know how to handle `ssh keys` pick that option, but otherwise pick the `password` option
    - backups and monitoring are optional
    - in `Finalize and Create` leave it as one droplet and give a simple hostname (like `bobwiki`) if you want.
    - click `create droplet`, wait for it to finish provisioning the droplet
2. Set up the DNS for your url so it points to the droplet IP address (I don't have instructions here, it is different for different domain registars)
3. in the list of droplets click `...` on the right in the entry for the new droplet and select `Access console` then click on `Launch Droplet Console`, a console will open in another window.
4. need to set up the filewall to open the ports needed
    - in the console type `sudo ufw allow http`
    - in the console type `sudo ufw allow https`
    - for this step you may need to create the root password, to do this on digital ocean go to the list of droplets you have, click on the droplet, on the left menu click `Access` then `Reset Root Password`, it will email you a new password, after that you will have to close and reopen the terminal and it will immediately prompt you to reset the password, then running the above commands should work, enter the new password if prompted.
5. snap is already installed on the docker image, but make sure that snap is installed and updated
    - in the terminal type `sudo snap install core; sudo snap refresh core`
6. install certbot
    - in the terminal type `sudo snap install --classic certbot`
    - then type `sudo ln -s /snap/bin/certbot /usr/bin/certbot`
7. get a certificate
    - type `sudo certbot certonly --standalone`
    - when asked put in the domain name (whatever your domain name is, something like `example.com`)
    - the process for renewing the certificates should automatically be set up as a cron job
8. get the repo with the docker files `git clone https://gitlab.com/ooktech-tw/bobdocker.git`
9. Set up basic authentication for nginx by setting a username and password
    - The easiest way to do this is to go here https://www.web2generators.com/apache-tools/htpasswd-generator, put in a username and password, then copy the output and paste it into the htpasswd file in this folder.
    - it looks something like this: `somename:$apr1$jszfumdp$51a0b8E0Ev4tfN9zH0boH1`
    - add the user
        - type `echo somename:$apr1$jszfumdp$51a0b8E0Ev4tfN9zH0boH1 >> htpasswd` where you use the result of the generate in place of `somename:$apr1$jszfumdp$51a0b8E0Ev4tfN9zH0boH1`
    - To add more users go to the site above and use the same command to add it to the htpasswd file, you can have as many as you want.
10. make all the configuration files use the correct domain name
    - in the terminal type `sed -i '.bak' 's/<DOMAIN NAME>/YOUR_DOMAIN_NAME/g' *` where instead of `YOUR_DOMAIN_NAME` you put in the domain name you are using, it will be something like `example.com`
11. build the bob docker image
    - type `docker build -t bobwiki .`
12. start everything
    - type `docker-compose up -d`

Now if you go to the url of your droplet you will be prompted for the username and password you used in step 8, after entering them you should be brought to your wiki and Bob should work like normal.

You can close the terminal window.
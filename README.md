# Bob with Docker

## DigitalOcean quick and easy way (with a domain name and https)

1. Create a droplet using the Docker image (make droplet->marketplace->docker image)
    - Use the basic plan with a regular ssd (you can pick other options, but the smallest one is plenty)
    - you don't need block storage, pick any datacenter region
    - if you know how to handle `ssh keys` pick that option, but otherwise pick the `password` option
    - backups and monitoring are optional
    - in `Finalize and Create` leave it as one droplet and give a simple hostname (like `bobwiki`) if you want.
    - click `create droplet`, wait for it to finish provisioning the droplet
    - in the list of droplets click the `...` on the right side in the entry for the new droplet. Click the `Reset root password` button, it will email you a temporary password for the root user, keep this for step 4.
2. Set up the DNS for your url so it points to the droplet IP address (I don't have instructions here, it is different for different domain registars). The dns propagation won't start until the server is up, but doing this here means when you are done you can just go to the wiki without any more steps.
3. in the list of droplets click `...` on the right in the entry for the new droplet and select `Access console` then click on `Launch Droplet Console`, a console will open in another window.
4. when prompted enter the password emailed to you in step 1, then enter a new root password. Save that password beacuse you will use it to log into the server if you ever need to in the future.
5. type `wget https://gitlab.com/ooktech-tw/bobdocker/-/raw/main/install.sh`
6. type `chmod +x ./install.sh`
7. type `./install.sh USERNAME PASSWORD DOMAIN`
    - replace `USERNAME` with the useranme you want
    - replace `PASSWORD` wth your password
    - replace `DOMAIN` with your domain (the url, something like `example.com`)
8. You will be asked for a contact email for the https certificate, so give one
9. When prompted for the domain name enter your domain name.
#!/bin/bash

USER=$1
PASSWORD=$2
DOMAIN=$3

BLORK=$(echo "$2" | md5sum | awk '{print $1}')

sudo ufw allow http
sudo ufw allow https

apt-get update
apt-get install apache2-utils -y

sudo snap install core; sudo snap refresh core

sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /user/bin/certbot

sudo certbot certonly --standalone

git clone https://gitlab.com/ooktech-tw/bobdocker.git

cd bobdocker
sed -i "s/<DOMAIN NAME>/$DOMAIN/g" *
htpasswd -nb $USER $PASSWORD >> htpasswd

docker build -t bobwiki .

docker-compose up -d

echo "Visit https://$DOMAIN to see your wiki, it may take some time to be available dute to DNS propagation delays."